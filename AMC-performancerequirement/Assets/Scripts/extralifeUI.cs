using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class extralifeUI : MonoBehaviour
{
    public Image lifeshow;
    public Sprite extralife;
    public Player pscript;
   

    
    void Update()
    {
        if (pscript.extraLife)
        {
            lifeshow.enabled = true;
            lifeshow.sprite = extralife;
        }
        else lifeshow.enabled = false;
    }
}
