using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class spriteUI : MonoBehaviour
{

    public Image image;
    public Sprite spriteA;
    public Sprite spriteB;
    public Player player;
    
    void Start()
    {
        
    }

    
    void Update()
    {
        if (player.AbilityA)
        {
            image.enabled = true;
            image.sprite = spriteA;
        }

        else if (player.AbilityB)
        {
            image.enabled = true;
            image.sprite = spriteB;
        }

        else image.enabled = false;
      
       
    }
}
