using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectile : MonoBehaviour
{
    public int prjspeed; 
    public Rigidbody2D rb;
    Vector3 lastVelocity;  
    private int collisions;
    void Start()
    {
        rb.velocity = transform.right * prjspeed;   // projectile moves "forward" from spawn point multiplied by projectile speed set in inspector 
    }

     void Update()
    {
        lastVelocity = rb.velocity;
        if( collisions == 3)
        {
            Destroy(gameObject);  // if projectile has bounced and collided 3 times, it gets deleted
    
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        collisions ++;
        var speed = lastVelocity.magnitude;
        var direction = Vector3.Reflect(lastVelocity.normalized, collision.contacts[0].normal);
        rb.velocity = direction * Mathf.Max(speed, 0f);  // function that detects the collisions of the projectile and reflects it along the normals of the collided object, with the same speed it had before it collided 

        
    }
}
