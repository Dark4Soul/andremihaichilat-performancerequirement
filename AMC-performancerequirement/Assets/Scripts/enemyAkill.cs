using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyAkill : MonoBehaviour
{
    public bool playerontop; // boolean which dictates if player is on top or not



    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerontop = true;
            print("player on top of enemy A");  // detects collision with player (this is a box collider on top of the enemy)
        }
    }
}
