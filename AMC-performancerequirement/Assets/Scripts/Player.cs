using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class Player : MonoBehaviour
{
    public float speed;  // float for adjusting player speed
    public Rigidbody2D rb;
    public float jumpforce;  // float for the force of the jump
    public GroundCheck check;  // reference to ground check script 
    public float speedmtp;  // speed multiplier for sprint function
    public Text uiScore;  // refernce to ui text display
    public int Score = 0;  // int for displaying score
    [HideInInspector] public bool AbilityA = false;
    [HideInInspector] public bool AbilityB = false;
    public Transform projectileshoot; // reference to empty from which projectile is shot
    public GameObject projectileOb; // reference to gameobject projectile 
    [HideInInspector] public bool extraLife = false; // boolean for extra life
    public float smashforce;  // float for adjusting the force of the stomp 
    [HideInInspector] public bool smashing = false; // boolean that decdies if the player can be killed based on if the player is stomping or not
    public int shootTimer = 2;  // int for adjusting the projectile cooldown
    [HideInInspector] public bool isShot = false;  // boolean that decides if the player can shoot projectile or not based on the cooldown time 
    [HideInInspector] public bool enemycooldown = false; // boolean that decides if the player can be hit again 
    public AudioSource powerBeffect;
    public AudioSource projectileeffect;
    public AudioSource gettinghit;
    public Animator anim;
    private bool isMoving = false;
    public GameObject bubble;
    
    
    
   

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
       
    }

    
    void Update()
    {
        StartCoroutine(Moving());
        uiScore.text = Score.ToString();  // converts integer score to string and displays on UI

        Vector3 mvm = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        transform.position += mvm * Time.deltaTime * speed; // left and right movement 

        if (smashing)
        {
            anim.SetTrigger("stomp");
            
        }

        if (isMoving && check.IsGrounded)
        {
            anim.SetBool("walk", true);
        }
        else anim.SetBool("walk", false);
       
        if (Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += mvm * Time.deltaTime * speed * speedmtp;  // if shift is pressed movement speed is multipled 
        }

        if (Input.GetKeyDown(KeyCode.Space) && check.IsGrounded)
        {
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpforce), ForceMode2D.Impulse); // jump function when space is pressed and player is on the ground 
            anim.SetTrigger("jump");
        }

        if (AbilityA && Input.GetKeyDown(KeyCode.E) && !isShot)  
        {
            Instantiate(projectileOb, projectileshoot.position, projectileshoot.rotation);
            projectileeffect.Play();
            isShot = true;
            StartCoroutine(PrShoot());  // script for shooting projectiles when E is pressed and the player has the ability + if the projectile is not on cooldown


        }
        

        if(AbilityB && Input.GetKeyDown(KeyCode.E) && !check.IsGrounded)
        {
            powerBeffect.Play();
            gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, -1f *smashforce), ForceMode2D.Impulse);
            smashing = true;
            print("is smashing");   // script for stomping the ground when E is pressed and the player is not on the ground
        }

      
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyA") && collision.transform.position.y >= transform.position.y && !extraLife && !enemycooldown)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // player dies and respawns if he gets hit by enemy A, excluding player jumping on enemy head, and if the player doesnt have an extra life + no enemy collision cooldown  
        }
        else if (collision.gameObject.CompareTag("EnemyA") && collision.transform.position.y >= transform.position.y && !smashing && !enemycooldown && extraLife)
        {
            PowerARevert();
            gettinghit.Play();
            StartCoroutine(Encd()); // if all the above but player has an extra life, player loses power A and loses extra life
        }
        else if (collision.gameObject.CompareTag("EnemyB") && !smashing && !enemycooldown && extraLife)
        {
            PowerARevert();
            gettinghit.Play();
            StartCoroutine(Encd()); // if all the above but player has an extra life, player loses power A and loses extra life
        }

        else if (collision.gameObject.CompareTag("EnemyA") && collision.transform.position.y >= transform.position.y && AbilityB && !smashing && !enemycooldown && extraLife)
        {
            PowerBRevert();
            gettinghit.Play();
            StartCoroutine(Encd());  // if all the above^2 but player has extra life, player loses power B and extra life
        }

        else if (collision.gameObject.CompareTag("EnemyB") && AbilityB && !smashing && !enemycooldown && extraLife)
        {
            PowerBRevert();
            gettinghit.Play();
            StartCoroutine(Encd());

        }
            if (collision.gameObject.CompareTag("EnemyB") && !extraLife && !smashing && !enemycooldown)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);  // all the above ^3 but on collision with enemy B which cannot be killed by jumping on top 
        }

        if(collision.gameObject.layer==3 && smashing)
        {
            StartCoroutine(StompBubble());
            smashing = false;
            print("not smashing"); // turns smashing boolean false which means after player stomps the ground
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("kz"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // killzone restart 
        }


        if (collision.gameObject.CompareTag("Coin1"))
        {
            Score += 1;
            Destroy(collision.gameObject);
        }
        else if (collision.gameObject.CompareTag("Coin2"))
        {
            Score += 5;
            Destroy(collision.gameObject);

        }
        else if (collision.gameObject.CompareTag("Coin3"))
        {
            Score += 10;
            Destroy(collision.gameObject);    // coin collection 
        }


        if (collision.gameObject.CompareTag("NextLevel"))
        {
            print("next scene");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // next level script
        }

        if (collision.gameObject.CompareTag("PowerA"))
        {
            PowerA();
            Destroy(collision.gameObject);
            AbilityB = false;   // power A collection
        }

        if (collision.gameObject.CompareTag("PowerB"))
        {
            PowerB();
            Destroy(collision.gameObject);
            AbilityA = false;    // power B collection 
        }
    }


    void PowerA()
    {
        print("powerup A");
        transform.localScale = new Vector2(1.5f, 1.5f);
        //show UI upgrade
        AbilityA = true;
        extraLife = true;   // power A script which scales the player, adds extra life and turns boolean ability A true
        

    }

    void PowerARevert()
    {
        print("PowerA lost");
        AbilityA = false;
        extraLife = false;
        transform.localScale = new Vector2(1, 1);  // script for losing power A when behing hit
       
    }
    
        
    void PowerB()
    {
        print("powerup B");
        transform.localScale = new Vector2(1.3f, 1.3f);
        AbilityB = true;
        extraLife = true;   // script for power A which scales the player, adds extra life and turns boolean ability B true

    }

   void PowerBRevert()
    {
        print("PowerB lost");
        AbilityB = false;
        extraLife = false;
        transform.localScale = new Vector2(1, 1); // script for losing power B whhen hit by enemey

    }


    IEnumerator PrShoot()
    {
        yield return new WaitForSeconds(shootTimer);
        isShot = false;  // coroutine which sets a cooldown for the projectiles and is adjustable with the shootTimer integer
    }

    IEnumerator Encd()
    {
        enemycooldown = true;
        yield return new WaitForSeconds(2);
        enemycooldown = false;   // enemy collision cooldown which wont allow for multiple rapid collisions resulting in the player dying immediately even with extra life
    }

    IEnumerator Moving()
    {
        var p1 = transform.position;
        yield return new WaitForSeconds(0.3f);
        var p2 = transform.position;

        isMoving = (p1 != p2);
    }

    IEnumerator StompBubble()
    {
        bubble.SetActive(true);
        yield return new WaitForSeconds(1);
        bubble.SetActive(false);
    }
}
