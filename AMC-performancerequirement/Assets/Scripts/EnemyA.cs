using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyA : MonoBehaviour
{
    public Rigidbody2D rb;
    public float enemyspeed; // float that determines the enemy A speed
    public enemyAkill EAK; // reference to boolean in script on the kill collider on top of enemy a
    private Vector2 spawn; // spawn point of enemy A
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        spawn = transform.position; // sets spawn point as the position at the start of the game 
    }

   
    void Update()
    {
        transform.Translate(-enemyspeed * Time.deltaTime, 0f, 0f); // moves the player towards the left 

        if (EAK.playerontop)
        {
            StartCoroutine(Death());   // if player is on top of the enemy this is detected in the referenced script(^) and kills the enemy
        }

        
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("kz"))
        {
            transform.position = spawn; // killzone death
        }
    }
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            StartCoroutine(Death());  // death triggered by projectile collision 
        }
    }


    IEnumerator Death()
    {
        print("Dying method");
        //play death animation
        yield return new WaitForSeconds(2);
        Destroy(gameObject);
        print("enemyA destroyed");   // death coroutine 
    }
}
