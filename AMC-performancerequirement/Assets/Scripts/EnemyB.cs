using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyB : MonoBehaviour
{
    [HideInInspector]public bool mustPatrol;
    public Rigidbody2D rb;
    public float enemyBspeed;  // adjustable speed of enemy b 
    private bool mustFlip;  // bool that controls the flipping of the character 
    public Transform groundcheck; // reference to groundcheck empty 
    public LayerMask ground; // reference to layer
    public Collider2D body; // reference to one of the colliders 
    public LayerMask Walls; // reference to layer 
    public Player script; // reference to player script 
    
    void Start()
    {
        mustPatrol = true;
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

   
    void Update()
    {
        if (mustPatrol)
        {
            Patrol();  
        }
    }

    private void FixedUpdate()
    {
        if (mustPatrol)
        {
            mustFlip = !Physics2D.OverlapCircle(groundcheck.position, 0.1f, ground); //if the enemy is patrolling and the ground check does not detect ground, the enemy has to flip/ go the other side
        }
    }


    void Patrol()
    {   
        if (mustFlip || body.IsTouchingLayers(Walls))
        {
            Flip();  // enemy flips if it meets an obstacle 
        }
        transform.Translate(enemyBspeed * Time.deltaTime, rb.velocity.y, 0f); // for the enemy movement in a direction
    }


     void Flip()
    {
        mustPatrol = false;
        transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
        enemyBspeed *= -1;
        mustPatrol = true;   // flips the enemy horizontally and "flips" the speed too to move with the direction
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Projectile"))
        {
            
            StartCoroutine(Death());  // if the enemy is hit by projectile, Death coroutine starts
        }

        if(collision.gameObject.CompareTag("Player") && script.smashing)
        {

            StartCoroutine(Death());  // if the enemy is hit by player from the stomp attack, Death starts
        }
    }

    IEnumerator Death()
    {
        mustPatrol = false;
        //play death animation 
        yield return new WaitForSeconds(2);
        Destroy(gameObject);   // plays death animation, waits 2 seconds then dies
    }
}
