using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camera : MonoBehaviour
{
    public Transform player;
    public float smooth = 0.1f;
    public Vector3 offset;


    void LateUpdate()
    {
        Vector3 camPos = player.position + offset;
        Vector3 posSmooth = Vector3.Lerp(transform.position, camPos, smooth);
        transform.position = posSmooth;
    }
}
